#include <stdio.h> //I/O support!
#include <windows.h>

int patchexecutable(char *filename)
{
	BYTE b;
	ULONG sum;
	FILE *f;
	long theEOF;
	f = fopen(filename,"rb+");
	if (!f) return 0; //Abort if not found!
	fseek(f, 0, SEEK_END);
	theEOF = ftell(f); //EOF!
	fseek(f,0,SEEK_SET); //Goto offset of our PE header location!
	sum = 0; //Init sum!
	while (!(ftell(f)==theEOF)) //Not EOF?
	{
		if (fread(&b, 1, 1, f) != 1) //Read the offset we need!
		{
			fclose(f);
			return 1; //Abort error!
		}
		if (!feof(f)) //Not reached EOF for this byte?
		{
			sum += (((ULONG)b) & 0xFF); //Sum it!
		}
	}
	sum &= 0xFF; //Mod 256!
	sum = 0x100 - sum; //Actual result!
	b = sum; //Set the result!
	fseek(f,ftell(f)-1, SEEK_SET); //Goto offset of our patch!
	fwrite(&b,1,1,f);
	fclose(f); //Finished!
	return 0; //OK!
}

// Our application entry point.
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	//Main builds
	//First, current directory!
	//First, 32-bit version!
	if (patchexecutable("PCXTBIOS_AT.bin")) return 1; //Error!

	return 0; //Finished!
}