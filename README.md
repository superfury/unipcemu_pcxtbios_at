# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
A project to add IBM PC AT BIOS functionality to the Generic Super PC/Turbo XT BIOS.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Currently none is used. It's just the assembler source code.
* Configuration
	* Compile the patch program using gcc and make.
	* Compile the ROM using NASM make.
	* Patch the ROM checksum using the patch program (generated in the same folder, at ../projects_build).
* Dependencies
* Database configuration
* How to run tests
Compile and use the ROM as a BIOS Option ROM(Address A0000-E0000) using a 64K ROM chip.
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact